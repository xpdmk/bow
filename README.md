# Bow
Bow is a self-hosted application for storing ideas and writing documents for later use. It uses React for frontend and an Express server.

Project is under irregular development.

## Get started
Set the port for application by duplicating `.env.template` file and renaming it to `.env`.
Optionally edit the `PORT` number to desired port to be used for the server.

Install `npm` globally if not already.

Before the application can be started you have to install the necessary packages and build the app. To do this, change directory to project root and run 
```
npm install
npm run build
```

After installing packages, start the server by running
```
npm start
```

Open browser and navigate to `localhost:[PORT]`, where `[PORT]` is the port that was set in `.env` (default is 3000)

## Features
Currently project has a test version of the ideas page as the front page at `/`. Project items can be removed by clicking the X button

Under `/editor` there is a test version of markdown editor that renders any markdown from the left textarea to the right side.