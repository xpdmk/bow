import React from 'react'
import ReactDOM from 'react-dom'
import Markdown from 'react-markdown'
import styled from 'styled-components'

const AreaContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const Area = styled.div`
  order: ${props => props.order || 1};
  margin: 15px;
  flex: 1;
`

const MarkdownTextArea = styled.textarea`
  font-family: Roboto Mono;
  resize: none;
  width: 100%;
  height: 100%;
  border-width: 0px;
`

const defaultMarkdown = 
`# Header
Some test text

## Subheader

Some more text` 

export default class EditorPage extends React.Component {
  constructor() {
    super()
    this.state = {
      markdown: defaultMarkdown
    }
  }

  render() {
    return (
      <AreaContainer>
        <Area order={1}>
          <MarkdownTextArea value={this.state.markdown} onChange={(event) => this.setState({
            markdown: event.target.value
          })}/>
        </Area>
        <Area order={2}>
          <Markdown source={this.state.markdown}/>
        </Area>
      </AreaContainer>)
  }
}
