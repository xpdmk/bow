import React, {Fragment} from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components'

import ProjectCard from "./../components/ProjectCard"
import ProjectCardContainer from './../components/ProjectCardContainer'

import projects from './../projects.json'

const ProjectCardStyler = styled.div `
	background-color: ${props => props.backgroundColor || 'white'};
	
	${ProjectCard} {
		max-width: 500px;
		margin-bottom: 10px;
		background-color: ${props => props.cardBackgroundColor || 'white'};
		filter: drop-shadow(0px 2px 2px #AAA);
		padding-bottom: 20px;
	}
`

export default class ProjectPage extends React.Component {
	render() {
		return (
			<Fragment>
				<ProjectCardStyler>
					<ProjectCardContainer projects={projects} />
				</ProjectCardStyler>
				<Link to="/editor">Temp link to editor page</Link>
			</Fragment>
		)
	}
}