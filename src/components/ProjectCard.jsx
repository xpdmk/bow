import React, {Component} from 'react'
import styled from 'styled-components'
import {ThemeProvider} from 'styled-components'
import * as icons from 'react-icons/lib/md'

const defaultTheme = {
	icon: {
		size: '25px',
		color: '#888',
		highlightColor: '#DDD'
	},
	topColor: '#DDD'
}

const TopContainer = styled.div `
	padding: 20px;
	padding-bottom: 10px;
	background-color: ${props => props.theme.topColor}
`
const Title = styled.h2 `
	margin: 0px;
`
const List = styled.ul `
	display: block;
	width: 100%;
	list-style-type: none;
  margin: 0;
  padding: 0;
`

const IconThemer =
	(props) => {
		const IconHighlighter = styled.span `
			display: inline-block;
			line-height: 100%;
			border-radius: 5px;
			background: rgba(0,0,0,0);
			${props => 
					props.highlight && props.theme.icon.highlightColor
						? `
								:hover {
									background-color: ${props.theme.icon.highlightColor};
								}
							`
						: ``
			}
		`

		// Create IconHighlighter object without props
		const {icon:Icon, ...IHProps} = props

		return (
			<IconHighlighter {...IHProps}>
				<Icon
					size={props.theme.icon.size || '20px'}
					color={props.theme.icon.color}
				/>
		</IconHighlighter>
		)
	}

const RightActions = styled.span `
	display: inline-block;
	vertical-align: middle;
	line-height: normal;
	float: right;
	svg + svg {
		padding-left: 5px;
	}
`

const ListItemText = styled.span `
	font-size: 16px;
	vertical-align: middle;
	line-height: normal;
	padding-left: 10px;
`;
const Row = styled.li `
	padding: 5px 18px 5px 18px;
	:hover {
		background-color: #EEE;
	}
`



class ProjectCard extends Component {
	createRow(task, index, theme) {
		return (
			<Row key={index}>
				<IconThemer
					icon={icons.MdMenu}
					theme={theme}
				/>
				<ListItemText>{task.title}</ListItemText>
				<RightActions>
					<IconThemer
						highlight
						icon={icons.MdDone}
						theme={theme}
					/>
					<IconThemer
						highlight
						icon={icons.MdClear}
						theme={theme}
						onClick={() => 
							this.props.removeTask(
								this.props.index,
								index
							)
						}
					/>
				</RightActions>
			</Row>)
	}

	render() {
		const theme = this.props.theme || defaultTheme;
		return (
			<ThemeProvider theme={theme}>
				<div className={this.props.className}>
					<TopContainer theme={theme}>
						<Title>{this.props.name}</Title>
					</TopContainer>
					<List>
						{this.props.tasks.map(
							(task, index) => this.createRow(task, index, theme))
						}
					</List>
				</div>
			</ThemeProvider>
		)
	}
}

export default styled(ProjectCard)``
export {defaultTheme, IconThemer}
