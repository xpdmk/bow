import React, {Component, Fragment} from 'react'
import ProjectCard from './ProjectCard'

// TODO: Comments

export default class ProjectCardContainer extends Component {
	constructor(props) {
		super(props)
		this.state = {
			projects: this.props.projects
		};

		// Binds the function to the object of this object
		this.removeTask = this.removeTask.bind(this)
	}

	removeTask(projectIndex, taskIndex) {
		this.state.projects[projectIndex].tasks.splice(taskIndex, 1)
		this.forceUpdate()
	}

	render() {
		return (
			<Fragment>
				{this.state.projects.map((project, index) => 
					<ProjectCard 
						key={index}
						index={index}
						name={project.name}
						tasks={project.tasks}
						removeTask={this.removeTask}
					/>)
				}
			</Fragment>)
	}
};
