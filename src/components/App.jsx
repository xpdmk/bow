import React, {Fragment} from "react"
import {Switch, Route} from "react-router-dom"
import {Helmet} from "react-helmet"

import Layout from "./../components/Layout"

import ProjectPage from "../pages/ProjectPage"
import EditorPage from "../pages/EditorPage"

const defaultStyle = {
  layout: {
    navigation: {
      backgroundColor: "green"
    }
  }
}

const baseStyle = `
  body {
    font-family: Roboto;
    margin: 0px;
  }
`

export default () =>
  <Fragment>
    <Helmet
					htmlAttributes={{lang: "en", amp: undefined}} // amp takes no value
					titleTemplate="%s | React App"
          titleAttributes={{itemprop: "name", lang: "en"}}
					meta={[
						{name: "description", content: "Server side rendering"},
						{name: "viewport", content: "width=device-width, initial-scale=1"},
          ]}
				>
      <title>Bow</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
      <style type="text/css">{baseStyle}</style>
    </Helmet>
    <Layout theme={defaultStyle.layout}>
      <Switch>
        <Route path="/" exact component={ProjectPage} />
        <Route path="/editor" component={EditorPage} />
      </Switch>
    </Layout>
  </Fragment>
