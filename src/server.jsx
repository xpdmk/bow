import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Helmet } from "react-helmet"
import Template from './template'
import App from './components/App'

export default () => {
	return (req, res, next) => {
		const markup = ReactDOMServer.renderToString(
			<StaticRouter location={ req.url } context={{}}>
				<App />
			</StaticRouter>)

		const helmet = Helmet.renderStatic()

		res.status(200).send(Template({
			markup,
			helmet
		}))
	}
}