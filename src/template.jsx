export default ({ markup, helmet }) => `
    <!DOCTYPE html>
    <html ${helmet.htmlAttributes.toString()}>
      <head>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${helmet.style.toString()}
      </head>
      <body ${helmet.bodyAttributes.toString()}>
        <div id="app">${markup}</div>
        <script src="/bundle/client.min.js" async></script>
      </body>
    </html>
  `